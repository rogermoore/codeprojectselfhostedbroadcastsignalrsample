﻿/// <reference path="../Scripts/jquery-1.10.2.js" />
/// <reference path="../Scripts/jquery.signalR-2.1.1.js" />

/*!
    ASP.NET SignalR Currency Exchange Sample
*/

// Crockford's supplant method (poor man's templating)
if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

// A simple background color flash effect that uses jQuery Color plugin
jQuery.fn.flash = function (color, duration) {
    var current = this.css('backgroundColor');
    this.animate({ backgroundColor: 'rgb(' + color + ')' }, duration / 2)
        .animate({ backgroundColor: current }, duration / 2);
};

$(function () {

    var ticker = $.connection.currencyExchangeService, // the generated client-side hub proxy
        up = '▲',
        down = '▼',
        $currencyTable = $('#currencyTable'),
        $currencyTableBody = $currencyTable.find('tbody'),
        rowTemplate = '<tr data-symbol="{CurrencySign}"><td>{CurrencySign}</td><td>{USDValue}</td><td>{Open}</td><td>{High}</td><td>{Low}</td><td><span class="dir {DirectionClass}">{Direction}</span> {RateChange}</td><td>{PercentChange}</td></tr>',
        currencyExchangeService = $('#currencyExchangeService'),
        $currencyExchangeUl = currencyExchangeService.find('ul'),
        liTemplate = '<li data-symbol="{CurrencySign}"><span class="symbol">{CurrencySign}</span> <span class="price">{USDValue}</span> <span class="rateChange"><span class="dir {DirectionClass}">{Direction}</span> {RateChange} ({PercentChange})</span></li>';

    function formatCurrency(currency) {
        return $.extend(currency, {
            USDValue: currency.USDValue.toFixed(2),
            PercentChange: (currency.PercentChange * 100).toFixed(2) + '%',
            Direction: currency.RateChange === 0 ? '' : currency.RateChange >= 0 ? up : down,
            DirectionClass: currency.RateChange === 0 ? 'even' : currency.RateChange >= 0 ? 'up' : 'down'
        });
    }

    function scrollTicker() {
        var w = $currencyExchangeUl.width();
        $currencyExchangeUl.css({ marginLeft: w });
        $currencyExchangeUl.animate({ marginLeft: -w }, 15000, 'linear', scrollTicker);
    }

    function stopTicker() {
        $currencyExchangeUl.stop();
    }

    function init() {
        return ticker.server.getAllCurrencies().done(function (currencies) {
            $currencyTableBody.empty();
            $currencyExchangeUl.empty();
            $.each(currencies, function () {
                var currency = formatCurrency(this);
                $currencyTableBody.append(rowTemplate.supplant(currency));
                $currencyExchangeUl.append(liTemplate.supplant(currency));
            });
        });
    }

    // Add client-side hub methods that the server will call
    $.extend(ticker.client, {
        updateCurrencyRate: function (currency) {
            var displayCurrency = formatCurrency(currency),
                $row = $(rowTemplate.supplant(displayCurrency)),
                $li = $(liTemplate.supplant(displayCurrency)),
                bg = currency.LastChange < 0
                        ? '255,148,148' // red
                        : '154,240,117'; // green

            $currencyTableBody.find('tr[data-symbol=' + currency.CurrencySign + ']')
                .replaceWith($row);
            $currencyExchangeUl.find('li[data-symbol=' + currency.CurrencySign + ']')
                .replaceWith($li);

            $row.flash(bg, 1000);
            $li.flash(bg, 1000);
        },

        marketOpened: function () {
            $("#open").prop("disabled", true);
            $("#close").prop("disabled", false);
            $("#reset").prop("disabled", true);
            scrollTicker();
        },

        marketClosed: function () {
            $("#open").prop("disabled", false);
            $("#close").prop("disabled", true);
            $("#reset").prop("disabled", false);
            stopTicker();
        },

        marketReset: function () {
            return init();
        }
    });

    // Start the connection
    $.connection.hub.start()
        .then(init)
        .then(function () {
            return ticker.server.getMarketState();
        })
        .done(function (state) {
            if (state === 'Open') {
                ticker.client.marketOpened();
            } else {
                ticker.client.marketClosed();
            }

            // Wire up the buttons
            $("#open").click(function () {
                ticker.server.openMarket();
            });

            $("#close").click(function () {
                ticker.server.closeMarket();
            });

            $("#reset").click(function () {
                ticker.server.reset();
            });
        });
});